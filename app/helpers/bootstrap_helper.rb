module BootstrapHelper
  LABEL_MAP = {
    "PENDING" => :warning,
    "PROCESSING" => :info,
    "COMPLETED" => :success
  }

  def bs_label(text, type = nil)
    html_class = 'label'
    type = LABEL_MAP[text.to_s.upcase] if type.nil?
    html_class += " label-#{type.to_s}" unless type.nil?
    content_tag :span, text.titleize, :class => html_class
  end

  def bs_badge(text, type = nil)
    html_class = 'badge'
    type = LABEL_MAP[text.to_s.upcase] if type.nil?
    html_class += " badge-#{type.to_s}" unless type.nil?
    content_tag :span, text.titleize, :class => html_class
  end

  def bs_nav_li(link, active = false, options = {})
    html_class = ''
    html_class = 'active' if active
    html_class += " dropdown" if options[:dropdown]
    html_class += " nav-header" if options[:header]
    content_tag :li, link, :class => html_class
  end

  def bs_icon(name, white = false, options = {})
    options[:class] ||= ""
    options[:class] += "glyphicon glyphicon-#{name.to_s}"
    options[:class] += " icon-white" if white
    content_tag :i, '', options
  end

  def bs_link_button(title, path, options = {})
    icon_options = { :white => false }

    options[:class] = "" if options[:class].nil?
    options[:class] += " btn"

    if options[:type]
      options[:class] += " btn-#{options[:type]}" if options[:type]
      icon_options[:white] = true
    end

    icon = ''
    icon = bs_icon(options[:icon], icon_options[:white]) unless options[:icon].nil?

    title = raw "#{icon} #{title}"

    link_to title, path, options
  end

  def bs_progress(value, options = {})
    content_tag :div, :class => "progress" do
      concat content_tag(:div, "", :class => "bar", :style => "width: #{value*100}%;" )
    end
  end

  def bs_check(bool)
    return content_tag(:i, "", :class => 'icon-ok') if bool
    return ""
  end

  def bs_alert(text, options = {})
    options[:type] ||= nil
    options[:hide_close] ||= false

    addtl_cls = ''
    addtl_cls = "alert-#{options[:type].to_s}" unless options[:type].nil?

    content_tag :div, :class => "alert #{addtl_cls}" do
      concat button_tag(raw('&times;'), :type => 'button', :class => 'close', :data => { :dismiss => 'alert' }) unless options[:hide_close]
      concat text
    end
  end

  def bs_dropdown(link, menu)
    label = content_tag(:a, :class => "dropdown-toggle", "data-toggle" => "dropdown", :role => "button") do
      concat link
      concat content_tag(:b, "", :class => "caret")
    end

    content_tag :li, :class => "dropdown"  do
      concat label
      concat bs_dropdown_menu(menu)
    end
  end

  def bs_dropdown_menu(menu)
    content_tag(:ul , :class => "dropdown-menu") do
      menu.each do |m|
        concat bs_nav_li(link_to((m[2]) ?  bs_icon(m[2]) + m[0].titleize : m[0].titleize, m[1]), current_controller?(m[0])) unless m[0] == :divider
        concat content_tag :li, '', :class => 'divider' if m[0] == :divider
      end
    end
  end
end
