module NavigationHelper
  def left_navigations
    links = ""

    links << bs_nav_li(
      button_tag(bs_icon('upload') + " Upload", class: "btn btn-primary", style: "margin-right: 20px;")
    )
    raw links
  end

  def right_navigations
    links = ""

    if current_user
      links << bs_nav_li(
        link_to(bs_icon('log-out') + " Logout #{current_user.name}", signout_path, id: 'sign_out', class: 'auto-tooltip', data: { toggle: 'tooltip', placement: 'bottom', title: 'Logout' }, style: "margin-right: 20px;")
      )
    else
      links << bs_nav_li(
        link_to(bs_icon('log-in') + " Login with Facebook" , "/auth/facebook", id: 'sign_in', class: 'auto-tooltip', data: { toggle: 'tooltip', placement: 'bottom', title: 'Sign in' }, style: "margin-right: 20px;")
      )
    end
    raw links
  end
end
