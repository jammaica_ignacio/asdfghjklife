class HomeController < ApplicationController
  def index
  end

  def post
    omniauth = session[:omniauth]
    token = omniauth["credentials"]["token"]
    me = FbGraph::User.me(token)
    me.photo!(
      :source => File.new('/home/jam/code/notable_quotes/test1.jpg'),
      :message => 'Yes. :)'
    )
  end
end
