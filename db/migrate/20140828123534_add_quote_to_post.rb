class AddQuoteToPost < ActiveRecord::Migration
  def self.up
    add_attachment :posts, :quote_image
  end

  def self.down
    remove_attachment :posts, :quote_image
  end
end
